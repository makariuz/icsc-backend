<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateStudentController extends AbstractController
{
    #[Route('/create/student', name: 'app_create_student')]
    public function index(): Response
    {
        return $this->render('create_student/index.html.twig', [
            'controller_name' => 'CreateStudentController',
        ]);
    }
}
